//
//  NetworkConnectionStatusService.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
import Combine
import Network

protocol NetworkConnectionStatusServiceInterface {
    var networkStatusPublisher: AnyPublisher<NWPath.Status, Never> { get }
    func startPublishingNetworkStatusChanges()
}

final class NetworkConnectionStatusService: NetworkConnectionStatusServiceInterface {
    
    var networkStatusPublisher: AnyPublisher<NWPath.Status, Never> {
        return networkStatusSubject.eraseToAnyPublisher()
    }
    
    private var subscriptionBag = Set<AnyCancellable>()
    private let nwPathMonitor: NWPathMonitorInterface
    private let monitorQueue: DispatchQueue = DispatchQueue(label: "nwPathMonitorQueue")
    private let networkStatusSubject = PassthroughSubject<NWPath.Status, Never>()
    
    init(nwPathMonitor: NWPathMonitorInterface = NWPathMonitor()) {
        self.nwPathMonitor = nwPathMonitor
    }
    
    func startPublishingNetworkStatusChanges() {
        nwPathMonitor
            .publisher(queue: monitorQueue)
            .sink { [weak self] status in
                self?.networkStatusSubject.send(status)
            }
            .store(in: &subscriptionBag)
    }
}
