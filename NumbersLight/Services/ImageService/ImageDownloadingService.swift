//
//  ImageDownloadingService.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import UIKit
import Combine

final class ImageDownloadingService {
    static let shared = ImageDownloadingService()
    private let imageCache: ImageCacheServiceInterface
    private var backgroundQueue: OperationQueue = OperationQueue()
    
    init(cache: ImageCacheServiceInterface = ImageCacheService()) {
        self.imageCache = cache
    }
    
    func downloadImage(from url: URL?) -> AnyPublisher<UIImage?, Never> {
        guard let url = url else {
            return Just(nil).eraseToAnyPublisher()
        }
        
        if let img = imageCache.getCachedImage(for: url) {
            return Just(img).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { (data, response) -> UIImage? in return UIImage(data: data) }
            .replaceError(with: nil)
            .handleEvents(receiveOutput: { [unowned self] downloadedImg in
                guard let img = downloadedImg else { return }
                imageCache.cacheImage(image: img, for: url)
            })
            .subscribe(on: backgroundQueue)
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
