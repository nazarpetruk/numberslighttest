//
//  ImageCacheService.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import UIKit

protocol ImageCacheServiceInterface: AnyObject {
    func getCachedImage(for url: URL) -> UIImage?
    func cacheImage(image: UIImage?, for url: URL)
}

final class ImageCacheService {
    private lazy var imgCache: NSCache<AnyObject, AnyObject> = {
        let cache = NSCache<AnyObject, AnyObject>()
        cache.totalCostLimit = 1024 * 1024 * 100
        cache.countLimit = 100
        return cache
    }()
    
    private let lock = NSLock()
}

extension ImageCacheService: ImageCacheServiceInterface {
    
    func cacheImage(image: UIImage?, for url: URL) {
        defer { lock.unlock() }
        
        guard let image = image else { return }
        let decodedImg = image.getDecodedImage()
        lock.lock()
        imgCache.setObject(decodedImg, forKey: url as AnyObject)
    }
    
    func getCachedImage(for url: URL) -> UIImage? {
        defer { lock.unlock() }
        lock.lock()
        
        if let img = imgCache.object(forKey: url as AnyObject) as? UIImage {
            return img
        } else {
            return nil
        }
    }
}
