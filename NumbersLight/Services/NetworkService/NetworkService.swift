//
//  NetworkService.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
import Combine

struct NetworkRequest {
    let url: String
    let timeout: Double?
    
    func createURLRequest(with url: URL) -> URLRequest {
        return URLRequest(url: url)
    }
}

enum NetworkError: Error {
    case badUrl
    case invalidJSON(description: String)
    case apiError(code: Int)
    case otherError(code: Int, comment: String)
}

protocol NetworkServiceInterface {
    func request<T>(request: NetworkRequest) -> AnyPublisher<T, NetworkError> where T: Codable
}

final class NetworkService: NetworkServiceInterface {
    private let timeout: Double = 0.5
    
    func request<T>(request: NetworkRequest) -> AnyPublisher<T, NetworkError> where T: Codable {
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.timeoutIntervalForRequest = TimeInterval(request.timeout ?? timeout)
        guard let url = URL(string: request.url) else { return AnyPublisher(Fail<T, NetworkError>(error: .badUrl))}
        return URLSession
            .shared
            .dataTaskPublisher(for: url)
            .tryMap { (data, response) -> (data: Data, response: URLResponse) in
                guard let response = response as? HTTPURLResponse else { throw NetworkError.otherError(code: 0, comment: "httpResponse is nil")}
                if  response.statusCode == 200 {
                    return (data, response)
                } else {
                    throw NetworkError.apiError(code: response.statusCode)
                }
            }
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in NetworkError.invalidJSON(description: error.localizedDescription) }
            .eraseToAnyPublisher()
    }
    
    init() { }
}
