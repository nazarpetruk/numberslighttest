//
//  NumbersService.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
import Combine

struct NumbersServiceRequestError: Error {
    let description: String
}

protocol NumbersServiceInterface {
    var numbersListPublisher: AnyPublisher<[ListItemModel], NumbersServiceRequestError> { get }
    var selectedNumberDetailsPublisher: AnyPublisher<ItemDetailsModel, NumbersServiceRequestError> { get }
    
    func requestNumbersList()
    func requestDetails(for numberName: String)
}

final class NumbersService: NumbersServiceInterface {
    
    var numbersListPublisher: AnyPublisher<[ListItemModel], NumbersServiceRequestError> { return numbersListSubject.eraseToAnyPublisher() }
    var selectedNumberDetailsPublisher: AnyPublisher<ItemDetailsModel, NumbersServiceRequestError> { return selectedNumberDetailsSubject.eraseToAnyPublisher() }
    
    private var numbersListSubject: PassthroughSubject<[ListItemModel], NumbersServiceRequestError> = PassthroughSubject<[ListItemModel], NumbersServiceRequestError>()
    private var selectedNumberDetailsSubject: PassthroughSubject<ItemDetailsModel, NumbersServiceRequestError> = PassthroughSubject<ItemDetailsModel, NumbersServiceRequestError>()
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let numberListURL: String = "http://dev.tapptic.com/test/json.php"
    private let detailsURL: String = "http://dev.tapptic.com/test/json.php?name="
    
    private let networkService: NetworkServiceInterface
    
    init(networkService: NetworkServiceInterface = NetworkService()) {
        self.networkService = networkService
    }
    
    func requestNumbersList() {
        let request = NetworkRequest(url: numberListURL, timeout: nil)
        
        networkService
            .request(request: request)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.hadleNumberListRequestError(error: error)
                case .finished:
                    return
                }
            }, receiveValue: { [weak self] numberList in
                self?.numbersListSubject.send(numberList)
            })
            .store(in: &subscriptionBag)
    }
    
    func requestDetails(for numberName: String) {
        let urlString = detailsURL + numberName
        let request = NetworkRequest(url: urlString, timeout: nil)
        
        networkService
            .request(request: request)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.handleSelectedNumberDetailsRequestError(error: error)
                case .finished:
                    return
                }
            } receiveValue: { [weak self] itemDetails in
                self?.selectedNumberDetailsSubject.send(itemDetails)
            }
            .store(in: &subscriptionBag)
    }
}

private extension NumbersService {
    
    //MARK: Request error handling
    //That two methods in real life would have diferent texts but for this project it will be enought
    
    func hadleNumberListRequestError(error: NetworkError) {
        let requestListError: NumbersServiceRequestError
        
        switch error {
        case .badUrl:
            requestListError = NumbersServiceRequestError(description: "Url for request was bad")
        case .invalidJSON(let description):
            requestListError = NumbersServiceRequestError(description: "JSON decoding failed with description: \(description)")
        case .apiError(let code):
            requestListError = NumbersServiceRequestError(description: "Api error with error code: \(code)")
        case .otherError(let code, let comment):
            requestListError = NumbersServiceRequestError(description: "Unexpected error with code: \(code), check details: \(comment)")
        }
        
        numbersListSubject.send(completion: .failure(requestListError))
    }
    
    func handleSelectedNumberDetailsRequestError(error: NetworkError) {
        let requestDetailsError: NumbersServiceRequestError
        
        switch error {
        case .badUrl:
            requestDetailsError = NumbersServiceRequestError(description: "Url for request was bad")
        case .invalidJSON(let description):
            requestDetailsError = NumbersServiceRequestError(description: "JSON decoding failed with description: \(description)")
        case .apiError(let code):
            requestDetailsError = NumbersServiceRequestError(description: "Api error with error code: \(code)")
        case .otherError(let code, let comment):
            requestDetailsError = NumbersServiceRequestError(description: "Unexpected error with code: \(code), check details: \(comment)")
        }
        
        selectedNumberDetailsSubject.send(completion: .failure(requestDetailsError))
    }
}
