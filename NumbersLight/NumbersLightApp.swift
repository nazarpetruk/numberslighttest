//
//  NumbersLightApp.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import SwiftUI

@main
struct NumbersLightApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
