//
//  UIImageExtension.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import UIKit

extension UIImage {
    
    func getDecodedImage() -> UIImage {
        guard let cgImg = cgImage else { return self }
        
        let size = CGSize(width: cgImg.width, height: cgImg.height)
        let context = CGContext(
                data: nil,
                width: Int(size.width),
                height: Int(size.height),
                bitsPerComponent: 8,
                bytesPerRow: cgImg.bytesPerRow,
                space: CGColorSpaceCreateDeviceRGB(),
                bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
        )
        context?.draw(cgImg, in: CGRect(origin: .zero, size: size))
        guard let decodedImg = context?.makeImage() else { return self }
        
        return UIImage(cgImage: decodedImg)
    }
}
