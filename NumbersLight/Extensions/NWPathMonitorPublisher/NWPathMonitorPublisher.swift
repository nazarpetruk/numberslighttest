//
//  NWPathMonitorPublisher.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
import Network
import Combine

extension NWPathMonitor {
    
    final class NetworkStatusSubscription<S: Subscriber>: Subscription where S.Input == NWPath.Status {
        
        private let subscriber: S?
        private let nwPathMonitor: NWPathMonitor
        private let queue: DispatchQueue
        
        init(
            subscriber: S,
            nwPathMonitor: NWPathMonitor,
            queue: DispatchQueue
        ) {
            self.subscriber = subscriber
            self.nwPathMonitor = nwPathMonitor
            self.queue = queue
        }
        
        func request(_ demand: Subscribers.Demand) {
            nwPathMonitor.pathUpdateHandler = { [weak self] path in
                _ = self?.subscriber?.receive(path.status)
            }
            
            nwPathMonitor.start(queue: queue)
        }
        
        func cancel() {
            nwPathMonitor.cancel()
        }
    }
}

extension NWPathMonitor {
    final class NetworkStatusPublisher: Publisher {
        
        typealias Output = NWPath.Status
        typealias Failure = Never
        
        private let nwPathMonitor: NWPathMonitor
        private let queue: DispatchQueue
        
        init(nwPathMonitor: NWPathMonitor, queue: DispatchQueue) {
            self.nwPathMonitor = nwPathMonitor
            self.queue = queue
        }
        
        func receive<S>(subscriber: S) where S : Subscriber, Never == S.Failure, NWPath.Status == S.Input {
            let subscription = NetworkStatusSubscription(subscriber: subscriber, nwPathMonitor: nwPathMonitor, queue: queue)
            subscriber.receive(subscription: subscription)
        }
    }
}

protocol NWPathMonitorInterface {
    func publisher(queue: DispatchQueue) -> NWPathMonitor.NetworkStatusPublisher
}

extension NWPathMonitor: NWPathMonitorInterface {
    
    func publisher(queue: DispatchQueue) -> NWPathMonitor.NetworkStatusPublisher {
        return NetworkStatusPublisher(nwPathMonitor: self, queue: queue)
    }
    
}
