//
//  ListItemModel.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
 
struct ListItemModel: Codable {
    let id: UUID = UUID()
    let name: String
    var image: String
    
    init(name: String, image: String) {
        self.name = name
        self.image = image.replacingOccurrences(of: "\"", with: "")
    }
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
    }
}
