//
//  ItemDetailsModel.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation

struct ItemDetailsModel: Codable {
    let name: String
    let text: String
    let image: String
    
    init(name: String, text: String, image: String) {
        self.name = name
        self.text = text
        self.image = image.replacingOccurrences(of: "\"", with: "")
    }
}
