//
//  AsyncImageViewModel.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import UIKit
import Combine

final class ImageBinder: ObservableObject {
    
    @Published private(set) var image: UIImage?
    private var subscriptionBag = Set<AnyCancellable>()
    private let defaultImg = UIImage(named: "noData")
    
    init() { }
    
    public func downloadImage(with url: URL?) {
        guard let defaultImg = defaultImg else { return }
        guard let url = url else {
            image = defaultImg
            return
        }
        
        ImageDownloadingService
            .shared
            .downloadImage(from: url)
            .replaceNil(with: defaultImg)
            .sink { [weak self] img in
                self?.image = img
            }
            .store(in: &subscriptionBag)
    }
}
