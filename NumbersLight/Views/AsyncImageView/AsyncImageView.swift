//
//  AsyncImageView.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import SwiftUI

struct AsyncImageView: View, Equatable {
    @ObservedObject private var imageBinder: ImageBinder = ImageBinder()
    var url: URL?
    
    static func == (lhs: AsyncImageView, rhs: AsyncImageView) -> Bool {
        lhs.url == rhs.url
    }
    
    init(url: URL?) {
        self.url = url
        self.imageBinder.downloadImage(with: url)
    }
    
    var body: some View {
        VStack {
            if let image = imageBinder.image {
                withAnimation {
                    Image(uiImage: image)
                        .resizable()
                }
            } else {
                withAnimation {
                    LoadingIndicatorView()
                        .padding()
                }
            }
        }
    }
}

struct AsyncImageView_Previews: PreviewProvider {
    static var previews: some View {
        AsyncImageView(url: URL(string: "https://img-9gag-fun.9cache.com/photo/a6O585q_700bwp.webp"))
    }
}
