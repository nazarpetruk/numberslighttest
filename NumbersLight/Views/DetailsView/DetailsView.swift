//
//  DetailsView.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 16/07/2022.
//

import SwiftUI

struct DetailsView: View {
    @ObservedObject var viewModel: DetailsViewModel
    var itemName: String
    
    init(itemName: String, viewModel: DetailsViewModel = DetailsViewModel()) {
        self.itemName = itemName
        self.viewModel = viewModel
        
        self.viewModel.getDetailsForNumber(with: itemName)
    }
    
    var body: some View {
        VStack {
            Text(viewModel.numberName)
                .font(.largeTitle)
            AsyncImageView(url: URL(string: viewModel.image))
                .clipShape(RoundedRectangle(cornerRadius: 25))
                .scaledToFit()
            Text(viewModel.detailsText)
                .font(.largeTitle)
        }
        .padding()
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailsView(itemName: "3")
    }
}
