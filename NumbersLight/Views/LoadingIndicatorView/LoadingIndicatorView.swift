//
//  LoadingIndicatorView.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import SwiftUI

struct LoadingIndicatorView: View {
    @State private var animate: Bool = false
    
    private let mainColor = Color.gray
    private let secondaryColor = Color.gray.opacity(0.5)
    
    var body: some View {
        activityIndicator
            .frame(maxWidth: 100, maxHeight: 100)
            .onAppear {
                withAnimation { self.animate.toggle() }
            }
    }
    
    private var activityIndicator: some View {
        ZStack {
            Circle()
                .trim(from: 0, to: 0.7)
                .stroke(
                    AngularGradient(gradient: .init(colors: [mainColor, secondaryColor]), center: .center),
                    style: StrokeStyle(lineWidth: 6, lineCap: .round)
                )
                .rotationEffect(Angle(degrees: animate ? 360 : 0))
                .animation(Animation.linear(duration: 0.5).repeatForever(autoreverses: false), value: animate)
        }
    }
}

struct LoadingIndicatorView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingIndicatorView()
    }
}

