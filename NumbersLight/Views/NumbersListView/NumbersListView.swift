//
//  NumbersListView.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 16/07/2022.
//

import SwiftUI

struct NumbersListView: View {
    @StateObject var viewModel: ListViewModel
    
    var body: some View {
        ZStack {
            NavigationView {
                List {
                    ForEach(viewModel.numberList, id: \.id) { item in
                        VStack {
                            NavigationLink(
                                destination: { DetailsView(itemName: item.name) },
                                label: { ListCellView(item: item) }
                            )
                        }
                    }
                }
                .listStyle(.inset)
                .navigationTitle(Text("Numbers"))
                .refreshable {
                    viewModel.reloadNumbersList()
                }
            }
            LoadingIndicatorView()
                .ignoresSafeArea()
                .opacity(viewModel.isReloading ? 1 : 0)
        }
        .onAppear {
            viewModel.reloadNumbersList()
        }
        .alert(Text("Smth went wrong"), isPresented: $viewModel.presentAlert) {
            Button("Ok",role: .cancel) { }
        } message: {
            Text(viewModel.alertMessage)
        }
    }
}

struct NumberListView_Previews: PreviewProvider {
    static var previews: some View {
        NumbersListView(viewModel: ListViewModel())
    }
}
