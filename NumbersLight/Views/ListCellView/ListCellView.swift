//
//  ListCellView.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 16/07/2022.
//

import SwiftUI

struct ListCellView: View {
    var item: ListItemModel
    
    var body: some View {
        HStack {
            Text(item.name)
                .font(.title2)
            Spacer()
            AsyncImageView(url: URL(string: item.image))
                .equatable()
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .frame(width: 50, height: 50)
        }
        .padding(.all, 12)
    }
}

struct ListCellView_Previews: PreviewProvider {
    static var previews: some View {
        ListCellView(item: ListItemModel(name: "1", image: "http://inu.tapptic.com/test/image.php?text=%E4%BA%8C"))
            .padding(.horizontal, 10)
    }
}
