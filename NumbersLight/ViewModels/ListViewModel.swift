//
//  ListViewModel.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
import Combine
import Network

final class ListViewModel: ObservableObject {
    
    @Published var numberList: [ListItemModel] = []
    @Published var presentAlert: Bool = false
    @Published var alertMessage: String = ""
    @Published var isReloading: Bool = false
    
    private var networkStatus: NWPath.Status = .satisfied {
        willSet {
            if networkStatus == .unsatisfied && newValue == .satisfied {
                reloadNumbersList()
            }
        }
    }
    private var subscriptionBag = Set<AnyCancellable>()
    
    private let networkConnectionStatusService: NetworkConnectionStatusServiceInterface
    private let numbersService: NumbersServiceInterface
    
    init(
        numberService: NumbersServiceInterface = NumbersService(),
        networkConnectionStatusService: NetworkConnectionStatusServiceInterface = NetworkConnectionStatusService()
    ) {
        self.numbersService = numberService
        self.networkConnectionStatusService = networkConnectionStatusService
        
        self.networkConnectionStatusService.startPublishingNetworkStatusChanges()
        subscribeToNetworkStatusPublisher()
        subscribeToNumbersListPublisher()
        reloadNumbersList()
    }
    
    func reloadNumbersList() {
        numbersService.requestNumbersList()
        isReloading = true
    }
}

private extension ListViewModel {
    
    func subscribeToNumbersListPublisher() {
        numbersService
            .numbersListPublisher
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.handleRequestError(error: error)
                case .finished:
                    return
                }
                self?.isReloading = false
            } receiveValue: { [weak self] itemList in
                self?.updateNumbersList(with: itemList)
            }
            .store(in: &subscriptionBag)
    }
    
    func handleRequestError(error: NumbersServiceRequestError) {
        alertMessage = error.description
        presentAlert = true
    }
    
    func updateNumbersList(with newList: [ListItemModel]) {
        numberList = newList
        isReloading = false
    }
    
    func subscribeToNetworkStatusPublisher() {
        networkConnectionStatusService
            .networkStatusPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] status in
                if status == .unsatisfied {
                    self?.presentUnsatisfiedNetworkConnectionAlert()
                }
                self?.networkStatus = status
            }
            .store(in: &subscriptionBag)
    }
    
    func presentUnsatisfiedNetworkConnectionAlert() {
        alertMessage = "No network connection!"
        presentAlert = true
    }
}
