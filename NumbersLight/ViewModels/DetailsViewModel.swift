//
//  DetailsViewModel.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import Foundation
import Combine
import Network

final class DetailsViewModel: ObservableObject {
    
    @Published var numberName: String = ""
    @Published var detailsText: String = ""
    @Published var image: String = "" 
    @Published var presentAlert: Bool = false
    @Published var isLoading: Bool = false
    @Published var alertMessage: String = ""
    
    private var subscriptionBag = Set<AnyCancellable>()
    private var lastSelectedNumberName: String = ""
    private var networkStatus: NWPath.Status = .satisfied {
        willSet {
            if networkStatus == .unsatisfied && newValue == .satisfied {
                getDetailsForNumber(with: lastSelectedNumberName)
            }
        }
    }
    private let numberService: NumbersServiceInterface
    private let networkConnectionStatusService: NetworkConnectionStatusServiceInterface
    
    init(
        numberService: NumbersServiceInterface = NumbersService(),
        networkConnectionStatusService: NetworkConnectionStatusServiceInterface = NetworkConnectionStatusService()
    ) {
        self.numberService = numberService
        self.networkConnectionStatusService = networkConnectionStatusService
        
        subscribeToDetailsPublisher()
        subscribeToNetworkConnectionStatusChanges()
    }
    
    func getDetailsForNumber(with name: String) {
        isLoading = true
        lastSelectedNumberName = name
        numberService.requestDetails(for: name)
    }
}

private extension DetailsViewModel {
    func subscribeToDetailsPublisher() {
        numberService
            .selectedNumberDetailsPublisher
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.handleDetailsRequestError(error: error)
                    self?.presentAlert = true
                case .finished:
                    return
                }
                self?.isLoading = false
            } receiveValue: { [weak self] details in
                self?.updateViewWithNumberDetails(details: details)
            }
            .store(in: &subscriptionBag)
    }
    
    func handleDetailsRequestError(error: NumbersServiceRequestError) {
        alertMessage = error.description
        presentAlert = true
    }
    
    func updateViewWithNumberDetails(details: ItemDetailsModel) {
        numberName = details.name
        image = details.image
        detailsText = details.text
    }
    
    func subscribeToNetworkConnectionStatusChanges() {
        networkConnectionStatusService
            .networkStatusPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] status in
                if status == .unsatisfied {
                    self?.presentUnsatisfiedNetworkConnectionAlert()
                }
            }
            .store(in: &subscriptionBag)
    }
    
    func presentUnsatisfiedNetworkConnectionAlert() {
        alertMessage = "No network connection!"
        presentAlert = true
    }
}
