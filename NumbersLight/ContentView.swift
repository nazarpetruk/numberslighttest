//
//  ContentView.swift
//  NumbersLight
//
//  Created by Nazar Petruk on 14/07/2022.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var viewModel: ListViewModel = ListViewModel()
    
    var body: some View {
        NumbersListView(viewModel: viewModel)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
