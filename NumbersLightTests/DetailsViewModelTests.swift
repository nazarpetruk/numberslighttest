//
//  DetailsViewModelTests.swift
//  NumbersLightTests
//
//  Created by Nazar Petruk on 18/07/2022.
//

import XCTest
import Combine
@testable import NumbersLight

class DetailsViewModelTests: XCTestCase {
    var sut: DetailsViewModel!
    var numberService: NumbersServiceMock!
    var networkConnectionStatusService: NetworkConnectionStatusServiceMock!

    override func setUpWithError() throws {
        try super.setUpWithError()
        
        numberService = NumbersServiceMock()
        networkConnectionStatusService = NetworkConnectionStatusServiceMock()
        
        sut = DetailsViewModel(numberService: numberService, networkConnectionStatusService: networkConnectionStatusService)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        
        numberService = nil
        networkConnectionStatusService = nil
        
        sut = nil
    }

    func testNewcomingDetailsUpdateViewModelProperties() {
        let itemName = "1"
        let itemText = "Ichi"
        let image = "http//..."
        let newItem = ItemDetailsModel(name: itemName, text: itemText, image: image)
        numberService.selectedNumberDetailsSubject.send(newItem)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.sut.numberName == itemName)
            XCTAssert(self.sut.detailsText == itemText)
            XCTAssert(self.sut.image == image)
        }
    }
    
    func testGetDetailsForNumberIsCallingGetDetails() {
        let numberName = "1"
        sut.getDetailsForNumber(with: numberName)
        
        XCTAssert(numberService.requestDetailsWasCalled)
        XCTAssert(sut.isLoading)
    }
    
    func testAlertWillBePresentedIfDetailsRequestFailed() {
        let errorDescription = "some awful error"
        numberService.selectedNumberDetailsSubject.send(completion: .failure(.init(description: errorDescription)))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.sut.alertMessage == errorDescription)
            XCTAssert(self.sut.presentAlert)
            XCTAssert(!self.sut.isLoading)
        }
    }
    
    func testAlertWillBePresentedIfNetworkConnectionStatusIsUnsatisfied() {
        networkConnectionStatusService.networkStatusSubject.send(.unsatisfied)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.sut.presentAlert)
            XCTAssert(self.sut.alertMessage != "")
        }
    }
    
    func testRequestNumberListAfterNetworkStatusBecameSatisfiedAgain() {
        networkConnectionStatusService.networkStatusSubject.send(.satisfied)
        networkConnectionStatusService.networkStatusSubject.send(.unsatisfied)
        networkConnectionStatusService.networkStatusSubject.send(.satisfied)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.numberService.requestNumberListWasCalled)
        }
    }

}
