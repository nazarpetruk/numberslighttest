//
//  NetworkConnectionStatusServiceMock.swift
//  NumbersLightTests
//
//  Created by Nazar Petruk on 17/07/2022.
//

import Foundation
import Combine
import Network
@testable import NumbersLight

final class NetworkConnectionStatusServiceMock: NetworkConnectionStatusServiceInterface {
    
    var startPublishingNetworkStatusChangesWasCalled: Bool = false
    
    var networkStatusSubject = PassthroughSubject<NWPath.Status, Never>()
    var networkStatusPublisher: AnyPublisher<NWPath.Status, Never> {
        return networkStatusSubject.eraseToAnyPublisher()
    }
    
    func startPublishingNetworkStatusChanges() {
        startPublishingNetworkStatusChangesWasCalled.toggle()
    }
}
