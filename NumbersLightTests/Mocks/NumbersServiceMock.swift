//
//  NumbersServiceMock.swift
//  NumbersLightTests
//
//  Created by Nazar Petruk on 17/07/2022.
//

import Foundation
import Combine
@testable import NumbersLight

final class NumbersServiceMock: NumbersServiceInterface {
    
    var requestNumberListWasCalled: Bool = false
    var requestDetailsWasCalled: Bool = false
    
    var numbersListPublisher: AnyPublisher<[ListItemModel], NumbersServiceRequestError> {
        return numberListSubject.eraseToAnyPublisher()
    }
    var numberListSubject = PassthroughSubject<[ListItemModel], NumbersServiceRequestError>()
    
    var selectedNumberDetailsPublisher: AnyPublisher<ItemDetailsModel, NumbersServiceRequestError> {
        return selectedNumberDetailsSubject.eraseToAnyPublisher()
    }
    var selectedNumberDetailsSubject = PassthroughSubject<ItemDetailsModel, NumbersServiceRequestError>()
    
    func requestNumbersList() {
        requestNumberListWasCalled = true
    }
    
    func requestDetails(for numberName: String) {
        requestDetailsWasCalled = true
    }
    
    
}
