//
//  ListViewModelTests.swift
//  NumbersLightTests
//
//  Created by Nazar Petruk on 17/07/2022.
//

import XCTest
@testable import NumbersLight

class ListViewModelTests: XCTestCase {
    var sut: ListViewModel!
    var numberService: NumbersServiceMock!
    var networkConnectionStatusService: NetworkConnectionStatusServiceMock!

    override func setUpWithError() throws {
        try super.setUpWithError()
        
        numberService = NumbersServiceMock()
        networkConnectionStatusService = NetworkConnectionStatusServiceMock()
        
        sut = ListViewModel(numberService: numberService, networkConnectionStatusService: networkConnectionStatusService)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        
        sut = nil
        numberService = nil
        networkConnectionStatusService = nil
    }
    
    func testNumberListRequestAndStartPublishingNetworkStatusWereCalledDuringInitialisation() {
        XCTAssert(numberService.requestNumberListWasCalled)
        XCTAssert(networkConnectionStatusService.startPublishingNetworkStatusChangesWasCalled)
    }
    
    func testAlertWillBePresentedIfRequestFailed() {
        let alertMessage = "some bad error"
        numberService.numberListSubject.send(completion: .failure(.init(description: alertMessage)))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.sut.presentAlert)
            XCTAssert(self.sut.alertMessage == alertMessage)
        }
    }
    
    func testIfReloadNumbersListWillRequestNumberList() {
        sut.reloadNumbersList()
        
        XCTAssert(sut.isReloading)
        XCTAssert(numberService.requestNumberListWasCalled)
    }
    
    func testAlertWillBePresentedIfNetworkConnectionStatusIsUnsatisfied() {
        networkConnectionStatusService.networkStatusSubject.send(.unsatisfied)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.sut.presentAlert)
            XCTAssert(self.sut.alertMessage != "")
        }
    }
    
    func testRequestNumberListAfterNetworkStatusBecameSatisfiedAgain() {
        networkConnectionStatusService.networkStatusSubject.send(.satisfied)
        networkConnectionStatusService.networkStatusSubject.send(.unsatisfied)
        networkConnectionStatusService.networkStatusSubject.send(.satisfied)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssert(self.numberService.requestNumberListWasCalled)
        }
    }
}
